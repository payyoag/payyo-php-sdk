# Payyo PHP SDK

The Payyo PHP SDK including an API client library

## Installation

    composer require payyo/php-sdk

## Configuration

### Creating a client

This library uses [PSR-18](https://packagist.org/providers/psr/http-client-implementation) (psr/http-client),
[PSR-17](https://packagist.org/providers/psr/http-factory-implementation) (psr/http-factory) and
[PSR-7](https://packagist.org/providers/psr/http-message-implementation) (psr/http-message).

To create a client you need to provide a request factory (`\Psr\Http\Message\RequestFactoryInterface`)
and an HTTP client (`\Psr\Http\Client\ClientInterface`) instance.

To make this part easier the SKD uses [php-http/discovery](https://php-http.readthedocs.io/en/latest/discovery.html)
to find available PSR-* implementations automatically.

    <?php
    use Payyo\Sdk\ApiClient\Client;
    use Payyo\Sdk\ApiClient\Credentials;
    
    $credentials = new Credentials(
        getenv('API_PUBLIC_KEY'),
        getenv('API_SECRET_KEY')
    );
    
    $apiClient = new Client($credentials);

You can however provide your own instances if desired.

    $httpClient = ...;      # \Psr\Http\Client\ClientInterface
    $requestFactory = ...;  # \Psr\Http\Message\RequestFactoryInterface
    
    $apiClient = new Client(
        $credentials,
        $httpClient,
        $requestFactory
    );

### API Version

The current version of this SDK targets [Payyo API version 3](https://developers.payyo.ch/releases/api/v3). The version can be configured using `withVersion()`.

    $apiClient = $apiClient->withVersion('2');

### Caching client

For caching you need to provide a [PSR-16](https://packagist.org/providers/psr/simple-cache-implementation) (psr/simple-cache) instance.

    $cache = ...;  # \Psr\SimpleCache\CacheInterface
    $ttl = 60;     # 1 minute
    
    $apiClient = $apiClient->withCache($ttl, $cache);
    
    // creating a new instance that caches for 2 minutes
    $apiClient = $apiClient->withCache(180);
    
    // creating a new instance that has caching disabled
    $apiClient = $apiClient->withCache(false);

### Logging client

For logging you need to provide a [PSR-3](https://packagist.org/providers/psr/log-implementation) (psr/log) instance.

    $logger = ...;  # \Psr\LoggerInterface    
    $apiClient = $apiClient->withLogger($logger);

## Usage

### Initiating a transaction

    $response = $apiClient->transaction()->initiate([
        'merchant_id'        => 123456, // use your merchant ID
        'merchant_reference' => 'test_order',
        'description'        => 'City Sightseeing',
        'currency'           => 'USD',
        'amount'             => 3000,   // => $30
        'return_urls' => [
            'success' => 'https://example.org/success',
            'fail'    => 'https://example.org/fail',
            'abort'   => 'https://example.org/abort',
        ],
        'funding_instrument' => [
            'type'    => 'credit_card',
            'number'  => '4242 4242 4242 4200',
            'expires' => '2024-04',
            'cvc'     => '123',
            'holder'  => 'John Doe',
        ]
    ]);
    
    handleResponse($response);
    
    // ---
    
    function handleResponse(Response $response): void {
        $transactionId = $response->getValue('result.transaction_id');
        
        switch ($response->getValue('result.next_action')) {
            case 'redirect':
                $redirectUrl = $response->getValue('result.redirect_url');
                // redirect to $redirectUrl for payment authentication ...
                break;
            case 'capture':
                $captureResponse = $apiClient->transaction()->capture($transactionId);
                handleResponse($captureResponse);
                break;
            case 'wait_for_webhook':
                // -> finish order, payment pending
                break;
            case 'none':
                // -> finish order, payment complete
                break;
        }
    }

### Retrieving next action for transaction

    $response = $apiClient->transaction()->getNextAction($transactionId);
    $transaction = $response->getValue('result');
    
    var_dump(
        $transaction['status'],
        $response->getValue('result.status')
    );

### Retrieving transaction details

    $response = $apiClient->transaction()->getDetails($transactionId);
    
    $nextAction = $response->getValue('result.next_action');
    // See example above with "handleResponse()"


### Caching requests

    $apiClientThatCachesForOneMinute = $apiClient->withCache(60);
    $response = $apiClientThatCachesForOneMinute->transaction()->getDetails($transactionId);

### Built-in RPC methods

The following methods are currently supported:

    $this->client->transaction()->getDccQuote(array $params);
    $this->client->transaction()->initiate(array $params);
    $this->client->transaction()->getNextAction(string $transactionId);
    $this->client->transaction()->void(string $transactionId);
    $this->client->transaction()->capture(string $transactionId, array $params);
    $this->client->transaction()->reverse(string $transactionId, array $params);
    $this->client->transaction()->abort(string $transactionId);
    $this->client->transaction()->getDetails(string $transactionId, array $params);
    
    $this->client->transactions()->search(int|int[] $merchantIds, array $params);
    $this->client->transactions()->getStats(int|int[] $merchantIds, array $params);
    
    $this->client->paymentPage()->initialize(array $params);
    $this->client->paymentPage()->getDetails(string $paymentPageId, array $params);
    $this->client->paymentPage()->search(int|int[] $merchantIds, array $params);

You can also call other RPC methods using the `request()` method.

    $this->client->request(string $method, array $params);

## Contributions

All contributions are welcomed through pull requests.

Please run tests (`vendor/bin/phpunit`) and coding style fixer (`composer run-cs-fixer`) before submitting.

## License

MIT. See LICENSE file.
