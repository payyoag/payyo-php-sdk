<?php

namespace Payyo\Sdk\ApiClient\Methods;

use Payyo\Sdk\ApiClient\Http\ResponseData;

final class Accounting extends MethodsCollection
{
    /**
     * @param int|int[] $merchantIdOrMerchantIds
     */
    public function searchAccounts($merchantIdOrMerchantIds, array $params = []): ResponseData
    {
        $params['merchant_ids'] = (array) $merchantIdOrMerchantIds;

        return $this->request('journal.searchAccounts', $params);
    }

    /** @deprecated Use listPayoutsForMerchant() instead */
    public function listPayoutStatementsForMerchant(int $merchantId, array $params = []): ResponseData
    {
        $params['merchant_ids'] = [$merchantId];

        return $this->request('payoutStatements.search', $params);
    }

    public function listPayoutsForMerchant(int $merchantId, array $params = []): ResponseData
    {
        $params['merchant_ids'] = [$merchantId];

        return $this->request('payouts.search', $params);
    }
}
