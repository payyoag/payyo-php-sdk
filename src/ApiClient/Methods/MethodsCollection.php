<?php

namespace Payyo\Sdk\ApiClient\Methods;

use Payyo\Sdk\ApiClient\Client;
use Payyo\Sdk\ApiClient\Http\ResponseData;

abstract class MethodsCollection
{
    /** @var Client */
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    protected function request(string $method, array $params): ResponseData
    {
        return $this->client->request($method, $params);
    }
}
