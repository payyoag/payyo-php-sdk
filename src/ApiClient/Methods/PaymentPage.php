<?php

namespace Payyo\Sdk\ApiClient\Methods;

use Payyo\Sdk\ApiClient\Http\ResponseData;

final class PaymentPage extends MethodsCollection
{
    public function getDetails(string $paymentPageId, array $params = []): ResponseData
    {
        $params['payment_page_id'] = $paymentPageId;

        return $this->request('paymentPage.getDetails', $params);
    }

    public function initialize(array $params): ResponseData
    {
        return $this->request('paymentPage.initialize', $params);
    }

    /**
     * @param int|int[] $merchantIdOrMerchantIds
     */
    public function search($merchantIdOrMerchantIds, array $params = []): ResponseData
    {
        $params['merchant_ids'] = (array) $merchantIdOrMerchantIds;

        return $this->request('paymentPages.search', $params);
    }
}
