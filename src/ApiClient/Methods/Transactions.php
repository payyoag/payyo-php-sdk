<?php

namespace Payyo\Sdk\ApiClient\Methods;

use Payyo\Sdk\ApiClient\Http\ResponseData;

final class Transactions extends MethodsCollection
{
    /**
     * @param int|int[] $merchantIdOrMerchantIds
     */
    public function search($merchantIdOrMerchantIds, array $params = []): ResponseData
    {
        $params['merchant_ids'] = (array) $merchantIdOrMerchantIds;

        return $this->request('transactions.search', $params);
    }

    /**
     * @param int|int[] $merchantIdOrMerchantIds
     */
    public function getStats($merchantIdOrMerchantIds, array $params = []): ResponseData
    {
        $params['merchant_ids'] = (array) $merchantIdOrMerchantIds;

        return $this->request('transactions.getStats', $params);
    }
}
