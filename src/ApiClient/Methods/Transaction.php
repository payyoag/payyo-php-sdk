<?php

namespace Payyo\Sdk\ApiClient\Methods;

use Payyo\Sdk\ApiClient\Http\ResponseData;

final class Transaction extends MethodsCollection
{
    public function getDetails(string $transactionId, array $params = []): ResponseData
    {
        $params['transaction_id'] = $transactionId;

        return $this->request('transaction.getDetails', $params);
    }

    public function getDccQuote(array $params): ResponseData
    {
        return $this->request('transaction.getDccQuote', $params);
    }

    public function capture(string $transactionId, array $params = []): ResponseData
    {
        $params['transaction_id'] = $transactionId;

        return $this->request('transaction.capture', $params);
    }

    public function getNextAction(string $transactionId): ResponseData
    {
        return $this->request('transaction.getNextAction', ['transaction_id' => $transactionId]);
    }

    public function refund(string $transactionId, array $params = []): ResponseData
    {
        $params['transaction_id'] = $transactionId;

        return $this->request('transaction.refund', $params);
    }

    public function reverse(string $transactionId, array $params = []): ResponseData
    {
        $params['transaction_id'] = $transactionId;

        return $this->request('transaction.reverse', $params);
    }

    public function void(string $transactionId): ResponseData
    {
        return $this->request('transaction.void', ['transaction_id' => $transactionId]);
    }

    public function abort(string $transactionId): ResponseData
    {
        return $this->request('transaction.abort', ['transaction_id' => $transactionId]);
    }

    /** @deprecated Use initiate() instead */
    public function initialize(array $params): ResponseData
    {
        return $this->request('transaction.initialize', $params);
    }

    /** @deprecated Use initiate() instead */
    public function authorize(array $params): ResponseData
    {
        return $this->request('transaction.authorize', $params);
    }

    /** @deprecated Use initiate() instead */
    public function pay(array $params): ResponseData
    {
        return $this->request('transaction.pay', $params);
    }
}
