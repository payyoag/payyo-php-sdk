<?php

namespace Payyo\Sdk\ApiClient\Http;

use Http\Factory\Guzzle\StreamFactory;
use Payyo\Sdk\ApiClient\Exceptions\OutOfBoundsException;
use Payyo\Sdk\ApiClient\Exceptions\RuntimeException;
use Payyo\Sdk\ApiClient\RequestError;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

final class ResponseData
{
    /** @var ResponseInterface */
    private $response;
    /** @var array|null */
    private $data;
    /** @var \DateTimeImmutable|null */
    private $cachedSince;

    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
    }

    public function getResponse(): ResponseInterface
    {
        return $this->response;
    }

    public function setCachedSince(\DateTimeImmutable $cachedSince): void
    {
        $this->cachedSince = $cachedSince;
    }

    public function getCachedSince(): ?\DateTimeImmutable
    {
        return $this->cachedSince;
    }

    /**
     * @throws ConnectionError
     * @throws RequestError
     */
    public function assertSuccessful(): void
    {
        $statusCode = $this->response->getStatusCode();

        if ($statusCode >= 200 && $statusCode < 300) {
            if (!$this->hasValue('error')) {
                return;
            }

            throw RequestError::create(
                $this->getValue('error.message'),
                $this->getValue('error.code'),
                $statusCode,
                (array)$this->getValueWithDefault('error.data', []),
                $this->getValueWithDefault('error.behavior', null)
            );
        }

        throw new ConnectionError('JSON-RPC request failed. Got status code '.$statusCode, $statusCode);
    }

    /**
     * @throws RuntimeException
     */
    public function getValues(): array
    {
        if ($this->data === null) {
            $this->data = $this->parseRequestBody($this->response->getBody());
        }

        return $this->data;
    }

    private function parseRequestBody(StreamInterface $body): array
    {
        $contents = $body->getContents();

        if ($contents === '') {
            return [];
        }

        $data = json_decode($contents, true);
        if (!is_array($data)) {
            throw new RuntimeException(
                'Failed to JSON decode server response: ' . json_last_error_msg() . '. Response body: ' . $contents
            );
        }

        return $data;
    }

    private function stringifyData(array $data): string
    {
        return json_encode($data);
    }

    public function getValue(string $key, string $separator = '.')
    {
        return $this->getValueRecursively(explode($separator, $key), $this->getValues());
    }

    public function getValueWithDefault(string $key, $default, string $separator = '.')
    {
        try {
            return $this->getValue($key, $separator);
        } catch (OutOfBoundsException $e) {
            return $default;
        }
    }

    public function hasValue(string $key, $separator = '.'): bool
    {
        try {
            $this->getValue($key, $separator);

            return true;
        } catch (OutOfBoundsException $e) {
            return false;
        }
    }

    /**
     * @return mixed
     *
     * @throws OutOfBoundsException
     */
    private function getValueRecursively(array $keys, array $data)
    {
        $key = array_shift($keys);

        if (array_key_exists($key, $data)) {
            if (0 === count($keys)) {
                return $data[$key];
            }

            return $this->getValueRecursively($keys, $data[$key]);
        }

        throw new OutOfBoundsException("Could not find key '$key' in data: ".print_r($data, true));
    }

    public function __sleep(): array
    {
        // ensure that the request body in parsed and `data` property is initialized
        $this->data = $this->getValues();
        $this->response->getBody()->close();

        return ['response', 'data', 'cachedSince'];
    }

    public function __wakeup(): void
    {
        $streamFactory = new StreamFactory();
        $body = $streamFactory->createStream($this->stringifyData($this->data));
        $body->rewind();
        $this->response = $this->response->withBody($body);
    }
}
