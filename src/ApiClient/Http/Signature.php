<?php

namespace Payyo\Sdk\ApiClient\Http;

use Base64Url\Base64Url;
use Payyo\Sdk\ApiClient\Credentials;
use Psr\Http\Message\RequestInterface;

final class Signature
{
    /** @var string */
    private $hash;

    /** @var Credentials */
    private $credentials;

    public static function forRequest(RequestInterface $request, Credentials $credentials): self
    {
        $body = $request->getBody();
        $body->rewind();

        $data = Base64Url::encode($body->getContents());

        $hash = hash_hmac('sha256', $data, $credentials->getSecretKey());

        return new self($hash, $credentials);
    }

    private function __construct(string $hash, Credentials $credentials)
    {
        $this->hash = $hash;
        $this->credentials = $credentials;
    }

    public function toString(): string
    {
        return $this->hash;
    }

    public function getAuthorizationHeader(): string
    {
        return sprintf(
            'Basic %s',
            base64_encode(sprintf(
                '%s:%s',
                $this->credentials->getPublicKey(),
                $this->hash
            ))
        );
    }
}
