<?php

namespace Payyo\Sdk\ApiClient\Http;

use Payyo\Sdk\ApiClient\Exceptions\RuntimeException;

class ConnectionError extends RuntimeException
{
}
