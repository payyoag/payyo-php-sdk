<?php

namespace Payyo\Sdk\ApiClient\Exceptions;

class OutOfBoundsException extends Exception
{
}
