<?php

namespace Payyo\Sdk\ApiClient\Exceptions;

class InvalidArgumentException extends Exception
{
}
