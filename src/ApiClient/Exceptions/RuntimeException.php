<?php

namespace Payyo\Sdk\ApiClient\Exceptions;

class RuntimeException extends Exception
{
}
