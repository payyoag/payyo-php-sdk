<?php

namespace Payyo\Sdk\ApiClient;

use Payyo\Sdk\ApiClient\Exceptions\RuntimeException;

class RequestError extends RuntimeException
{
    public const BEHAVIOR_DO_NOT_RETRY = 'do_not_retry';
    public const BEHAVIOR_RETRY_LATER = 'retry_later';

    /** @var int */
    private $statusCode = 0;
    /** @var string|null */
    private $behavior;
    /** @var array */
    private $details = [];
    /** @var array */
    private $debugInfo = [];

    public static function create(
        string $errorMessage,
        int $errorCode,
        int $statusCode,
        array $details,
        ?string $behavior = null,
        array $debugInfo = []
    ): self {
        $e = new self($errorMessage, $errorCode);
        $e->behavior = $behavior;
        $e->statusCode = $statusCode;
        $e->details = $details;
        $e->debugInfo = $debugInfo;

        return $e;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getDetails(): array
    {
        return $this->details;
    }

    public function getDebugInfo(): array
    {
        return $this->debugInfo;
    }

    public function getBehavior(): ?string
    {
        return $this->behavior;
    }
}
