<?php

namespace Payyo\Sdk\ApiClient;

use Http\Discovery\Psr17FactoryDiscovery;
use Http\Discovery\Psr18ClientDiscovery;
use Payyo\Sdk\ApiClient\Http\ConnectionError;
use Payyo\Sdk\ApiClient\Http\ResponseData;
use Payyo\Sdk\ApiClient\Http\Signature;
use Payyo\Sdk\ApiClient\Methods\Accounting;
use Payyo\Sdk\ApiClient\Methods\PaymentPage;
use Payyo\Sdk\ApiClient\Methods\Transaction;
use Payyo\Sdk\ApiClient\Methods\Transactions;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Psr\SimpleCache\CacheInterface;

class Client
{
    /** @var string */
    private $baseUrl = 'https://api.payyo.ch';
    /** @var string */
    private $version = '3';
    /** @var Credentials */
    private $credentials;
    /** @var ClientInterface */
    private $httpClient;
    /** @var RequestFactoryInterface */
    private $requestFactory;
    /** @var CacheInterface|null */
    private $cache;
    /** @var bool */
    private $useCache = false;
    /** @var int */
    private $cacheTtl = 30;
    /** @var string|null */
    private $correlationId;
    /** @var array */
    private $meta = [];
    /** @var string|null */
    private $metaHash;
    /** @var ErrorCodeSpecificLogger */
    private $logger;

    public function __construct(Credentials $credentials, ?ClientInterface $httpClient = null, ?RequestFactoryInterface $requestFactory = null)
    {
        $this->credentials = $credentials;
        $this->httpClient = $httpClient ?: Psr18ClientDiscovery::find();
        $this->requestFactory = $requestFactory ?: Psr17FactoryDiscovery::findRequestFactory();
        $this->logger = new ErrorCodeSpecificLogger(new NullLogger());
    }

    /**
     * We don't clone the cache as it may have been referenced from the outside to gather information.
     */
    public function __clone()
    {
        $this->httpClient = clone $this->httpClient;
        $this->requestFactory = clone $this->requestFactory;
        $this->credentials = clone $this->credentials;
        $this->logger = clone $this->logger;
    }

    public function withNewCredentials(Credentials $credentials): self
    {
        $clone = clone $this;
        $clone->credentials = $credentials;

        return $clone;
    }

    public function withVersion(string $version): self
    {
        $clone = clone $this;
        $clone->version = $version;

        return $clone;
    }

    public function withOtherBaseUrl(string $baseUrl): self
    {
        $clone = clone $this;
        $clone->baseUrl = $baseUrl;

        return $clone;
    }

    public function withLogger(LoggerInterface $logger): self
    {
        $clone = clone $this;
        $clone->logger = $logger instanceof ErrorCodeSpecificLogger ? $logger : new ErrorCodeSpecificLogger($logger);

        return $clone;
    }

    public function withCorrelationId(string $correlationId): self
    {
        $clone = clone $this;
        $clone->correlationId = $correlationId;

        return $clone;
    }

    public function withMeta(array $meta, string $hash): self
    {
        if (0 === count($meta) || empty($hash)) {
            return $this;
        }

        $clone = clone $this;
        $clone->meta = $meta;
        $clone->metaHash = $hash;

        return $clone;
    }

    public function getCache(): ?CacheInterface
    {
        return $this->cache;
    }

    /**
     * @param int|bool|null $ttl
     *
     * @return $this
     */
    public function withCache($ttl = null, ?CacheInterface $cache = null): self
    {
        $clone = clone $this;
        if ($cache) {
            $clone->cache = $cache;
        }

        if (false === $ttl) {
            $clone->useCache = false;
        } else {
            $clone->useCache = true;

            if (null !== $ttl) {
                $clone->cacheTtl = $ttl;
            }
        }

        return $clone;
    }

    public function transaction(): Transaction
    {
        return new Transaction($this);
    }

    public function transactions(): Transactions
    {
        return new Transactions($this);
    }

    public function accounting(): Accounting
    {
        return new Accounting($this);
    }

    public function paymentPage(): PaymentPage
    {
        return new PaymentPage($this);
    }

    /**
     * @param string $method
     * @param array $params
     * @return ResponseData
     * @throws ConnectionError
     * @throws RequestError
     */
    public function request(string $method, array $params = []): ResponseData
    {
        $cacheKey = $this->generateCacheKey($method, $params);

        if (count($this->meta) > 0 && !empty($this->metaHash)) {
            $params['@meta'] = $this->meta;
            $params['@meta_hash'] = $this->metaHash;
        }

        $data = json_encode([
            'jsonrpc' => '2.0',
            'method' => $method,
            'params' => new \ArrayObject($params),
            'id' => 1,
        ], JSON_THROW_ON_ERROR);

        if ($this->useCache && $response = $this->cache->get($cacheKey)) {
            return $response;
        }

        $request = $this->requestFactory->createRequest('POST', $this->baseUrl.'/v'.$this->version);
        $request->getBody()->write($data);
        $request->getBody()->rewind();

        $response = $this->doRequest($request);

        if ($this->useCache) {
            $response->setCachedSince(new \DateTimeImmutable());
            $this->cache->set($cacheKey, $response, $this->cacheTtl);
        }

        return $response;
    }

    private function generateCacheKey(string $method, array $params): string
    {
        $data = json_encode([
            'method' => $method,
            'params' => new \ArrayObject($params),
        ], JSON_THROW_ON_ERROR);

        return md5($this->credentials->getPublicKey().':'.$data);
    }

    /**
     * @param RequestInterface $request
     * @return ResponseData
     * @throws ConnectionError
     * @throws RequestError
     */
    private function doRequest(RequestInterface $request): ResponseData
    {
        $this->logger->info("Request: {$request->getMethod()} {$request->getRequestTarget()}");

        $signature = Signature::forRequest($request, $this->credentials);
        $this->logger->debug('Request signature: '.$signature->toString());

        $request = $request->withHeader('Authorization', $signature->getAuthorizationHeader());
        $request = $request->withHeader('Accept', 'application/json');
        $request = $request->withHeader('Content-Type', 'application/json');

        if (null !== $this->correlationId) {
            $request = $request->withHeader('X-Correlation-ID', $this->correlationId);
        }

        try {
            $response = new ResponseData($this->httpClient->sendRequest($request));
            $response->assertSuccessful();
        } catch (RequestError $e) {
            $this->logger->logRequestError($e);

            throw $e;
        } catch (ClientExceptionInterface $e) {
            throw new ConnectionError($e->getMessage(), $e->getCode(), $e);
        }

        return $response;
    }
}
