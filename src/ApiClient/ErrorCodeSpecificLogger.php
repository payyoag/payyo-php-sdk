<?php

namespace Payyo\Sdk\ApiClient;

use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

class ErrorCodeSpecificLogger extends AbstractLogger
{
    /** @var LoggerInterface */
    private $logger;
    /** @var string */
    private $defaultErrorLogLevel = LogLevel::ERROR;
    /** @var string[] */
    private $logLevelsByErrorCode = [];

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function setDefaultErrorLogLevel(string $logLevel): void
    {
        $this->defaultErrorLogLevel = $logLevel;
    }

    public function setErrorSpecificLogLevel(int $errorCode, string $logLevel): void
    {
        $this->logLevelsByErrorCode[$errorCode] = $logLevel;
    }

    public function logRequestError(RequestError $e): void
    {
        $logLevel = array_key_exists($e->getCode(), $this->logLevelsByErrorCode)
            ? $this->logLevelsByErrorCode[$e->getCode()]
            : $this->defaultErrorLogLevel;

        $this->log($logLevel, "Request failed: {$e->getMessage()} (E{$e->getCode()})", [
            'message' => $e->getMessage(),
            'code' => $e->getCode(),
            'details' => $e->getDetails(),
        ]);
    }

    public function log($level, $message, array $context = []): void
    {
        $this->logger->log($level, $message, $context);
    }
}
