# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [8.0.0] - 2025-03-10
### Added
- Added support for PHP 8.4
### Removed
- Dropped support for PHP 8.2

## [7.1.0] - 2024-03-27
### Added
- Added `RequestError->getBehavior()` with values `RequestError::BEHAVIOR_*` that indicate whether a failed request can be retried or not

## [7.0.0] - 2024-03-27
### Removed
- Dropped support for PHP 7, 8.0 and 8.1

## [6.1.1] - 2022-09-16
### Added
- Added some methods documentation

## [6.1.0] - 2022-08-08
### Added
- Added support for `payouts.search`

### Changed
- Deprecated `payoutStatements.search` API method

## [6.0.0] - 2021-08-06
### Added
- Added support for PHP 8.0

## [5.0.1] - 2021-01-15
### Changed
- Added support for serialization of responses

## [5.0.0] - 2020-09-09
### Added
- Added support for PHP 7.4 by fixing ext-json requirement. See https://github.com/lcobucci/content-negotiation-middleware/issues/31#issuecomment-547817869 for decision on using "*".

### Changed
- Changed namespace from `TrekkPay` to `Payyo`
- Deprecated transaction methods initialize(), authorize() and pay()
- Updated dependencies to latest versions

### Removed
- Dropped support for PHP 7.1
- Removed Monolog dependency (only dev dependency now)
- Removed Guzzle dependency
- Removed Doctrine Cache dependency
- Removed paratest dependency
- Removed `CredentialsStorage` class

## [4.0.0] - 2020-08-06
### Changed
- Changed base URL from `trekkpay.io` to `payyo.ch`
- Bumped default API version from 2 to 3

## [3.0.1] - 2020-02-17
### Changed
- Logger no longer logs request body as it may contain sensitive information
 
## [3.0.0] - 2019-09-25
### Added
- Added support for API versioning

### Changed
- Bumped default API version from 1 to 2
