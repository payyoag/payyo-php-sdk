<?php

declare(strict_types=1);

use Payyo\Sdk\ApiClient\RequestError;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

use function PHPUnit\Framework\assertSame;

final class RequestErrorTest extends TestCase
{
    #[Test]
    public function create_error(): void
    {
        $response = RequestError::create(
            'error message',
            123,
            400,
            ['details'],
            RequestError::BEHAVIOR_DO_NOT_RETRY,
            ['debugInfo']
        );

        assertSame('error message', $response->getMessage());
        assertSame(123, $response->getCode());
        assertSame(400, $response->getStatusCode());
        assertSame(['details'], $response->getDetails());
        assertSame(RequestError::BEHAVIOR_DO_NOT_RETRY, $response->getBehavior());
        assertSame(['debugInfo'], $response->getDebugInfo());
    }

    #[Test]
    public function create_error_without_behavior_parameter(): void
    {
        $response = RequestError::create(
            'error message',
            123,
            400,
            ['details'],
            null,
            ['debugInfo']
        );

        assertSame('error message', $response->getMessage());
        assertSame(123, $response->getCode());
        assertSame(400, $response->getStatusCode());
        assertSame(['details'], $response->getDetails());
        assertSame(null, $response->getBehavior());
        assertSame(['debugInfo'], $response->getDebugInfo());
    }
}
