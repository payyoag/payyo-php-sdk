<?php

namespace Payyo\Sdk\Tests\ApiClient;

use GuzzleHttp\Psr7\Response;
use Http\Factory\Guzzle\RequestFactory;
use Payyo\Sdk\ApiClient\Client;
use Payyo\Sdk\ApiClient\Credentials;
use Payyo\Sdk\ApiClient\Http\ConnectionError;
use Payyo\Sdk\ApiClient\RequestError;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Adapter\TraceableAdapter;
use Symfony\Component\Cache\Psr16Cache;

final class ClientTest extends TestCase
{
    private ClientInterface $httpClient;
    private RequestFactoryInterface $requestFactory;
    private Client $client;

    #[Test]
    public function i_can_create_a_caching_client(): void
    {
        $cache = new TraceableAdapter(new ArrayAdapter());
        $client = $this->client->withCache(60, new Psr16Cache($cache));

        $countHits = static function () use ($cache): int {
            $i = 0;
            foreach ($cache->getCalls() as $c) {
                if ($c->name === 'getItem') {
                    $i++;
                }
            }

            return $i;
        };

        $countMisses = static function () use ($cache): int {
            $i = 0;
            foreach ($cache->getCalls() as $c) {
                if ($c->name === 'getItem') {
                    $i++;
                }
            }

            return $i;
        };

        // write
        $client->transaction()->getDetails('tra_2f4bbf77c39017ba6b7bc9298672');
        self::assertSame(1, $countHits());
        self::assertSame(1, $countMisses());

        $cache->clearCalls();

        // read
        $client->transaction()->getDetails('tra_2f4bbf77c39017ba6b7bc9298672');
        self::assertSame(1, $countHits());
        $client->transaction()->getDetails('tra_2f4bbf77c39017ba6b7bc9298672');
        self::assertSame(2, $countHits());
        $client->transaction()->getDetails('tra_2f4bbf77c39017ba6b7bc9298672');
        self::assertSame(3, $countHits());
    }

    #[Test]
    public function i_can_make_successful_requests(): void
    {
        $response = $this->client->transaction()->getDetails('tra_2f4bbf77c39017ba6b7bc9298672');

        self::assertSame($response->getValue('jsonrpc'), '2.0');
        self::assertSame($response->getValue('result.transaction_id'), 'tra_2f4bbf77c39017ba6b7bc9298672');
        self::assertSame($response->getValue('result.status'), 'authorized');
        self::assertSame($response->getValue('id'), 1);
    }

    #[Test]
    public function i_handle_user_errors_properly(): void
    {
        $this->expectExceptionMessage('Merchant not allowed');
        $this->expectException(RequestError::class);

        $this->client->transactions()->search(1);
    }

    #[Test]
    public function i_handle_connection_errors_properly(): void
    {
        $this->expectExceptionMessage('JSON-RPC request failed. Got status code 500');
        $this->expectException(ConnectionError::class);

        $this->client->transactions()->getStats(1);
    }

    public function setUp(): void
    {
        $this->httpClient = new class() implements ClientInterface {
            public function sendRequest(RequestInterface $request): ResponseInterface
            {
                $body = $request->getBody();
                $body->rewind();
                $data = json_decode($body->getContents());

                switch ($data->method) {
                    case 'transaction.getDetails':
                        return new Response(200, ['contant-type' => 'application/json'], json_encode([
                            'jsonrpc' => '2.0',
                            'result' => [
                                'transaction_id' => 'tra_2f4bbf77c39017ba6b7bc9298672',
                                'status' => 'authorized',
                            ],
                            'id' => $data->id,
                        ], JSON_THROW_ON_ERROR));

                    case 'transactions.search':
                        return new Response(200, ['contant-type' => 'application/json'], json_encode([
                            'jsonrpc' => '2.0',
                            'error' => [
                                'code' => -32602,
                                'message' => 'Merchant not allowed',
                            ],
                            'id' => $data->id,
                        ], JSON_THROW_ON_ERROR));

                    case 'transactions.getStats':
                        return new Response(500, [], 'Internal servier error');

                    default:
                        throw new \LogicException('Not implemented');
                }
            }
        };

        $this->requestFactory = new RequestFactory();

        $this->client = new Client(
            new Credentials('', ''),
            $this->httpClient,
            $this->requestFactory
        );
    }
}
