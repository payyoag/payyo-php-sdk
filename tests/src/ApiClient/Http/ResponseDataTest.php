<?php

namespace Payyo\Sdk\Tests\ApiClient\Http;

use GuzzleHttp\Psr7\Response;
use Payyo\Sdk\ApiClient\Http\ResponseData;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

final class ResponseDataTest extends TestCase
{
    #[Test]
    public function request_data_seeps_correctly(): void
    {
        $response = new Response(200, [], '');
        $messageData = new ResponseData($response);

        $excludedProps = [];
        $serializableProps = $messageData->__sleep();
        $reflect = new \ReflectionClass($messageData);
        $allProps = array_map(
            static function (\ReflectionProperty $p) {
                return $p->getName();
            },
            $reflect->getProperties(
                \ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED | \ReflectionProperty::IS_PRIVATE
            )
        );

        $diffProps = array_diff($allProps, $serializableProps);
        self::assertEquals($excludedProps, $diffProps, 'Not all class properties are serialized');
    }

    #[Test]
    public function request_data_can_be_serialized_and_unserialized(): void
    {
        $now = new \DateTimeImmutable();
        $data = ['pro' => 'val'];
        $body = json_encode($data);
        $response = new Response(201, [], $body);
        $messageData = new ResponseData($response);
        $messageData->setCachedSince($now);

        /** @var ResponseData $deserialized */
        $deserialized = unserialize(serialize($messageData));

        self::assertEquals($now, $deserialized->getCachedSince());
        self::assertEquals($data, $deserialized->getValues());

        self::assertNotSame($response, $deserialized->getResponse());
        self::assertEquals($response->getStatusCode(), $deserialized->getResponse()->getStatusCode());
        self::assertEquals($body, $deserialized->getResponse()->getBody()->getContents());
    }
}
