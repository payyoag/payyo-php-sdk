<?php

namespace Payyo\Sdk\Tests\ApiClient\Http;

use GuzzleHttp\Psr7\Request;
use Payyo\Sdk\ApiClient\Credentials;
use Payyo\Sdk\ApiClient\Http\Signature;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

final class SignatureTest extends TestCase
{
    #[Test]
    public function request_signature_is_calculated_correctly(): void
    {
        $data = json_encode([
            'jsonrpc' => '2.0',
            'method' => 'transaction.getDetails',
            'params' => [
                'transaction_id' => 'tra_2f4bbf77c39017ba6b7bc9298672',
            ],
            'id' => 1,
        ]);

        $request = new Request('POST', '/v1');
        $request->getBody()->write($data);
        $request->getBody()->rewind();

        $signature = Signature::forRequest($request, new Credentials(
            'api_3f77c29ba6b7b86729017bbc92f4',
            'sec_22f4b9867017ba6b7bf77c39bc92'
        ));

        self::assertSame(
            '55ed3a3fcebb8a5d089555804ca57565e4dc949b4406ef735546161a9fa574bd',
            $signature->toString()
        );

        self::assertSame(
            'Basic YXBpXzNmNzdjMjliYTZiN2I4NjcyOTAxN2JiYzkyZjQ6NTVlZDNhM2ZjZWJiOGE1ZDA4OTU1NTgwNGNhNTc1NjVlNGRjOTQ5YjQ0MDZlZjczNTU0NjE2MWE5ZmE1NzRiZA==',
            $signature->getAuthorizationHeader()
        );
    }
}
