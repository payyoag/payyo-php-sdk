<?php

namespace Payyo\Sdk\Tests\ApiClient;

use Monolog\Handler\TestHandler;
use Monolog\Logger;
use Payyo\Sdk\ApiClient\ErrorCodeSpecificLogger;
use Payyo\Sdk\ApiClient\RequestError;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Psr\Log\LogLevel;

final class ErrorCodeSpecificLoggerTest extends TestCase
{
    /** @var TestHandler */
    private $recorder;

    /** @var ErrorCodeSpecificLogger */
    private $logger;

    public function setUp(): void
    {
        $this->recorder = new TestHandler();

        $this->logger = new ErrorCodeSpecificLogger(
            new Logger('test', [$this->recorder])
        );
    }

    #[Test]
    public function i_get_all_errors_by_default(): void
    {
        $this->logRequestErrors();

        self::assertTrue($this->recorder->hasErrorThatContains('Invalid card data'));
        self::assertTrue($this->recorder->hasErrorThatContains('No terminal available'));
        self::assertTrue($this->recorder->hasErrorThatContains('Internal server error'));
    }

    #[Test]
    public function i_can_change_the_default_error_level(): void
    {
        $this->logger->setDefaultErrorLogLevel(LogLevel::WARNING);
        $this->logRequestErrors();

        self::assertTrue($this->recorder->hasWarningThatContains('Invalid card data'));
        self::assertFalse($this->recorder->hasErrorThatContains('Invalid card data'));
        self::assertTrue($this->recorder->hasWarningThatContains('No terminal available'));
        self::assertTrue($this->recorder->hasWarningThatContains('Internal server error'));
    }

    #[Test]
    public function i_can_change_the_default_error_level_and_define_error_specific_log_levels(): void
    {
        $this->logger->setDefaultErrorLogLevel(LogLevel::ALERT);
        $this->logger->setErrorSpecificLogLevel(1103, LogLevel::INFO);
        $this->logger->setErrorSpecificLogLevel(1006, LogLevel::DEBUG);
        $this->logRequestErrors();

        self::assertTrue($this->recorder->hasInfoThatContains('Invalid card data'));
        self::assertFalse($this->recorder->hasErrorThatContains('Invalid card data'));
        self::assertFalse($this->recorder->hasAlertThatContains('Invalid card data'));
        self::assertTrue($this->recorder->hasDebugThatContains('No terminal available'));
        self::assertTrue($this->recorder->hasAlertThatContains('Internal server error'));
    }

    private function logRequestErrors(): void
    {
        $this->logger->logRequestError(RequestError::create('Invalid card data', 1103, 200, []));
        $this->logger->logRequestError(RequestError::create('No terminal available', 1006, 200, []));
        $this->logger->logRequestError(RequestError::create('Internal server error', 1199, 200, []));
    }
}
